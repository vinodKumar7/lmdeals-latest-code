//
//  MyDealsViewController.m
//  LMDeals
//
//  Created by vairat on 24/06/15.
//  Copyright (c) 2015 vairat. All rights reserved.
//

#import "MyDealsViewController.h"
//float degrees = 45.0;
//float radians = (degrees/180.0) * M_PI;
#define DegreesToRadians(x) ((x) * M_PI / 180.0)

@interface MyDealsViewController (){
    
    CGPoint point;
    BOOL expand;
}

@end

@implementation MyDealsViewController
@synthesize view1, view2, view3;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"My Deals";
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
    {
        UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
        [self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
        
        
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ButtonMenu"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self.navigationController.parentViewController action:@selector(revealToggle:)];
        
        expand = NO;
        
        
  self.view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 8, 25, 3)];
  self.view2 = [[UIView alloc]initWithFrame:CGRectMake(0, 15, 25, 3)];
  self.view3 = [[UIView alloc]initWithFrame:CGRectMake(0, 22, 25, 3)];
        
        
        self.view1.backgroundColor = [UIColor orangeColor];
        self.view2.backgroundColor = [UIColor orangeColor];
        self.view3.backgroundColor = [UIColor orangeColor];
        
        
        
        UIView *menuView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        menuView.backgroundColor = [UIColor clearColor];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapAction:)];
        [menuView addGestureRecognizer:tapRecognizer];
   
        [menuView addSubview:self.view1];
        [menuView addSubview:self.view2];
        [menuView addSubview:self.view3];
    

        
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuView];
        
        self.navigationItem.rightBarButtonItem = barButtonItem;
        //[[[self navigationController] navigationItem] setRightBarButtonItem:barButtonItem];
        
        
        
        
        
        
        
        
       // NSLog(@"center X: %f",view1.layer.)
        NSLog(@"%f %f",view1.frame.origin.x,view1.frame.origin.y);
        NSLog(@"%f %f",view2.frame.origin.x,view2.frame.origin.y);
        NSLog(@"----------------------------");
        
       // self.view1.layer.anchorPoint = CGPointMake(0.5, 0.5);
       // self.view2.layer.anchorPoint = CGPointMake(0.5, 0.5);
        NSLog(@"%f %f",self.view1.center.x,self.view1.center.y);
        
        point = CGPointMake(self.view2.center.x, self.view2.center.y);
    }
}

- (void)onTapAction:(UITapGestureRecognizer*)recognizer
{
    
    if(!expand)
    [UIView animateWithDuration:0.20
                     animations:^{
                         view1.transform = CGAffineTransformMakeRotation(DegreesToRadians(48));
                         view2.transform = CGAffineTransformMakeRotation(-DegreesToRadians(48));
                         
                         
                         self.view2.center = self.view1.center;
                         view3.alpha = 0;
                         
                         }
                     completion:^(BOOL finished){
                         expand = YES;
                     }];
    else
        [UIView animateWithDuration:0.25
                         animations:^{
                             
                             view1.transform = CGAffineTransformIdentity;
                             view2.transform = CGAffineTransformIdentity;
                             
                             self.view2.center = point;
                             view3.alpha = 1;
                         }
                         completion:^(BOOL finished){
                             
                             expand = NO;
                             
                         }];
}






-(IBAction)rotate:(id)sender{
    
    
    [UIView animateWithDuration:0.20
                     animations:^{
                         view1.transform = CGAffineTransformMakeRotation(DegreesToRadians(48));
                         view2.transform = CGAffineTransformMakeRotation(-DegreesToRadians(48));
                         
                         
                         self.view2.center = self.view1.center;
                         view3.alpha = 0;
                         
                     }
                     completion:^(BOOL finished){
                        
                         
                         
                     }];
    
    
    
}
-(IBAction)rotate2:(id)sender{
    
    [UIView animateWithDuration:0.25
                     animations:^{

                                view1.transform = CGAffineTransformIdentity;
                                view2.transform = CGAffineTransformIdentity;
                         
                         self.view2.center = point;
                         view3.alpha = 1;
                     }
                     completion:^(BOOL finished){
                         
                         
                         
                     }];
    

    
}








- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

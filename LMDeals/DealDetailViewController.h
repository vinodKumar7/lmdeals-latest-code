//
//  DealDetailViewController.h
//  LMDeals
//
//  Created by vairat on 23/06/15.
//  Copyright (c) 2015 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressCell.h"
#import "ProductDataParser.h"
#import "MerchantListParser.h"
#import "RedeemXMLparser.h"

@interface DealDetailViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate,ProductXMLParserDelegate,  MerchantXMLParserDelegate, RedeemXMLparserDelegate>
{
    int phnoCount;
    NSMutableArray *phMutableArray;
    NSArray *phArray;
    
    CLLocationCoordinate2D Location;
}

@property (strong, nonatomic) IBOutlet UIView *buttonsBaseView;
@property (strong, nonatomic) IBOutlet UICollectionView *productCollectionView;

@property (strong, nonatomic) IBOutlet AddressCell *cCell;
@property (strong, nonatomic) NSString *product_ID;
@property (strong, nonatomic) IBOutlet UIButton *favBtnOutlet;
@property (strong, nonatomic) UIView *loadingView;

- (IBAction)headerBtnPressed:(id)sender;
- (IBAction)favBtnPressed:(id)sender;


@end

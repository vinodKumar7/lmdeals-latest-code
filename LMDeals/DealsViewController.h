//
//  DealsViewController.h
//  LMDeals
//
//  Created by vairat on 22/06/15.
//  Copyright (c) 2015 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductListParser.h"
#import <MapKit/MapKit.h>

@interface DealsViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource, ProductListXMLParserDelegate, UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate>
{
    NSMutableArray *productImagesArray;
}

@property(nonatomic, retain) UIView *view1;
@property(nonatomic, retain) UIView *view2;
@property(nonatomic, retain) UIView *view3;


@property(nonatomic, strong)IBOutlet UICollectionView *deals_CollectionView;
@property (strong, nonatomic) NSMutableArray *productsList;



@end

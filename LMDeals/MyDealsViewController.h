//
//  MyDealsViewController.h
//  LMDeals
//
//  Created by vairat on 24/06/15.
//  Copyright (c) 2015 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyDealsViewController : UIViewController



@property(nonatomic, retain) UIView *view1;
@property(nonatomic, retain) UIView *view2;
@property(nonatomic, retain) UIView *view3;

-(IBAction)rotate:(id)sender;
-(IBAction)rotate2:(id)sender;

@end

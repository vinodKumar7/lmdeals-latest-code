//
//  DealsTableCell.h
//  LMDeals
//
//  Created by vairat on 27/06/15.
//  Copyright (c) 2015 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DealsTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *productNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *productImgView;

@end

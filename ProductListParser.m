//
//  ProductListParser.m
//  GrabItNow
//
//  Created by MyRewards on 12/23/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "ProductListParser.h"
#import "Product.h"
#import <CoreLocation/CoreLocation.h>
#define Image_URL_Prefix_Loyalty @"http://184.107.152.53/files/product_image/"

@interface ProductListParser()
{
    NSMutableArray *productsList;
    NSMutableString *charString;
    Product *currProduct;
}

@property (nonatomic, strong) NSMutableArray *productsList;

@end


@implementation ProductListParser
@synthesize delegate;
@synthesize productsList;


- (void)parserDidStartDocument:(NSXMLParser *)parser {
    productsList = [[NSMutableArray alloc] init];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    charString = nil;
    
    if ([elementName isEqualToString:@"product"]) {
        currProduct = [[Product alloc] init];
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if (!charString) {
        charString = [[NSMutableString alloc] initWithString:string];
    }
    else {
        [charString appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {

    NSString *finalString = [charString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([elementName isEqualToString:@"id"]) {
        currProduct.productId = finalString;
        
        // construct image link
//        NSString *imgLink = [NSString stringWithFormat:@"%@%@.jpg",Image_URL_Prefix,finalString];
//        NSLog(@"imageLink %@",imgLink);
//        currProduct.productImgLink = imgLink;
        
        
    }
    else if ([elementName isEqualToString:@"name"]) {
        currProduct.productName = finalString;
    }
    else if ([elementName isEqualToString:@"highlight"]) {
        currProduct.productOffer = finalString;
    }
    else if ([elementName isEqualToString:@"latitude"]) {
        
        CLLocationCoordinate2D localCoo = currProduct.coordinate;
        localCoo.latitude = [finalString doubleValue];
        currProduct.coordinate = localCoo;
    }
    else if ([elementName isEqualToString:@"longitude"]) {
        
        CLLocationCoordinate2D localCoo = currProduct.coordinate;
        localCoo.longitude = [finalString doubleValue];
        currProduct.coordinate = localCoo;
    }
    else if ([elementName isEqualToString:@"pin_type"]) {
        currProduct.pin_type = finalString;
    }
    else if ([elementName isEqualToString:@"text"]) {
        
        currProduct.productText = finalString;
    }
    else if ([elementName isEqualToString:@"quantity"])
    {
        currProduct.quantity = finalString;
    }
    else if ([elementName isEqualToString:@"text"])
    {
        
        currProduct.productText = finalString;
    }

    else if ([elementName isEqualToString:@"app_image_extension"] || [elementName isEqualToString:@"image_extension"]) {
        
        currProduct.appImageExtension = finalString;
        
        NSLog(@"appImage Extension %@",currProduct.appImageExtension);
    }
    else if ([elementName isEqualToString:@"product"]) {
        
//        NSString *imgLink = [NSString stringWithFormat:@"%@%@.jpg",Image_URL_Prefix,finalString];
        currProduct.carouselImgLink = [NSString stringWithFormat:@"%@%@.jpg",Image_URL_Prefix_Loyalty,currProduct.productId];
        
//        [NSString stringWithFormat:@"http://184.107.152.53/files/product_image/%@.jpg",currProduct.productId,currProduct.appImageExtension];
        
        NSLog(@"carouselImgLink::%@",currProduct.carouselImgLink);
        
        [productsList addObject:currProduct];
        currProduct = nil;
        
    }
    else if([elementName isEqualToString:@"root"]||[elementName isEqualToString:@"roots"]) {
        [parser abortParsing];
        [delegate parsingProductListFinished:productsList];
        
    }
    
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    [delegate parsingProductListXMLFailed];
}

@end

/*
 <root>
    <product>
        <id>1016469</id>
        <name>American Tourister Luggage</name>
        <highlight>Save up to $100!</highlight>
    </product>
    <link_next>search.php?cid=24&amp;q=&amp;start=10&amp;limit=10</link_next>
    <link_prev></link_prev>
 </root>
 
 */

